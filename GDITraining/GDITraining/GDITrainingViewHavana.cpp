#include "stdafx.h"

#ifndef SHARED_HANDLERS
#include "GDITraining.h"
#endif

#include "GDITrainingDoc.h"
#include "GDITrainingView.h"

#define toRad 0.01745

void CGDITrainingView::DrawHavanaField(CDC* pDC, CPoint ptCenter, float sideLen, COLORREF tile)
{
	POINT points[6];

	CBrush* brush = new CBrush(RGB(255,255,0));
	CBrush* brushTile = new CBrush(tile);
	CBrush* oldBrush = pDC->SelectObject(brush);

	float angle = 0.0;
	for (int i = 0; i < 6; i++)
	{
		points[i].x = ptCenter.x + sideLen * cos(angle*toRad);
		points[i].y = ptCenter.y + sideLen * sin(angle*toRad);
		angle += 60.0;
	}

	pDC->Polygon(points, 6);

	if (tile != NULL)
	{
		pDC->SelectObject(brushTile);
		float h = sideLen / 2 * sqrt(3.0);
		pDC->Ellipse(-h + ptCenter.x, -h + ptCenter.y, h + ptCenter.x, h + ptCenter.y);
	}

	pDC->SelectObject(oldBrush);
	brush->DeleteObject();
	brushTile->DeleteObject();
}

void CGDITrainingView::DrawHavanaBoard(CDC* pDC, CPoint topLeft, int sideElements, float sideLen)
{
	XFORM oldTrans;
	pDC->GetWorldTransform(&oldTrans);

	float h = sideLen / 2 * sqrt(3.0);

	DrawHavanaBackground(pDC, CPoint(topLeft.x - 1 * sideLen, topLeft.y - 1 * sideLen),
		CPoint(topLeft.x + 4 * h * sideElements - 2 * sideLen, topLeft.y + 4 * h * sideElements - 2 * sideLen), CString("grey.jpg"));

	Translate(pDC, topLeft.x, topLeft.y + (sideElements - 1) * h, false);

	int rowIncrement = 0;
	for (int i = 0; i < 2*sideElements-1; i++)
	{
		for (int j = 0; j < sideElements + rowIncrement; j++)
		{
			int rnd = rand() % 100;
			if(rnd < 50)
				DrawHavanaField(pDC, CPoint(0, j * 2 * h), sideLen, NULL);
			else if(rnd < 75)
				DrawHavanaField(pDC, CPoint(0, j * 2 * h), sideLen, RGB(255, 0, 0));
			else
				DrawHavanaField(pDC, CPoint(0, j * 2 * h), sideLen, RGB(0,0,255));
		}
		
		if (i < sideElements - 1)
		{
			Translate(pDC, 2 * h, -h, false);
			rowIncrement += 1;
		}
		else
		{
			Translate(pDC, 2 * h, h, false);
			rowIncrement -= 1;
		}
		
	}

	pDC->SetWorldTransform(&oldTrans);
}

void CGDITrainingView::DrawHavanaBackground(CDC* pDC, CPoint topLeft, CPoint bottomRight, CString strPicture)
{
	DImage* img = new DImage();
	img->Load(strPicture);

	float dx = bottomRight.x - topLeft.x;
	float dy = bottomRight.y - topLeft.y;

	//img->Draw(pDC, CRect(0, 0, img->Width(), img->Height()), CRect(topLeft, CPoint(bottomRight.x + dx / 2, bottomRight.y + dy / 2)));

	img->Draw(pDC, CRect(0, 0, img->Width(), img->Height()), CRect(topLeft, CPoint(topLeft.x + dx / 2, topLeft.y + dy / 2) ));
	img->Draw(pDC, CRect(0, 0, img->Width(), img->Height()), CRect(CPoint(topLeft.x + dx / 2, topLeft.y), CPoint(bottomRight.x, bottomRight.y - dy / 2) ));
	img->Draw(pDC, CRect(0, 0, img->Width(), img->Height()), CRect(CPoint(topLeft.x, topLeft.y + dy / 2), CPoint(bottomRight.x - dx / 2, bottomRight.y) ));
	img->Draw(pDC, CRect(0, 0, img->Width(), img->Height()), CRect(CPoint(topLeft.x + dx / 2, topLeft.y + dy / 2), bottomRight));
}