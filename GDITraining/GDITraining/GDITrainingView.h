
// GDITrainingView.h : interface of the CGDITrainingView class
//
#include"DImage.h"

#pragma once


class CGDITrainingView : public CView
{
protected: // create from serialization only
	CGDITrainingView() noexcept;
	DECLARE_DYNCREATE(CGDITrainingView)

// Attributes
public:
	CGDITrainingDoc* GetDocument() const;

protected:
	double offsetX;
	double offsetY;
	char letters[9];

	float m_hPos;
	float m_angle;
	float d_alpha;
	DImage* m_pTocak;
	HENHMETAFILE m_met;

	float wheelDistance;

// Operations
public:
	void DrawAxes(CDC* pDC);
	void PrimeniTransformaciju(CDC* pDC, float cx, float cy, float rx, float ry, float alfa, XFORM* oldXForm);
	void LoadIdentity(CDC* pDC);
	void Translate(CDC* pDC, float dX, float dY, bool rightMul);
	void Rotate(CDC* pDC, float angle, bool rightMul);
	void Scale(CDC* pDC, float sX, float sY, bool rightMul);
	void Mirror(CDC* pDC, bool x, bool right);
	void DrawRoom(CDC* pDC);
	void DrawBoard(CDC* pDC, float from_X, float from_Y, float to_X, float to_Y);
	void DrawWheel(CDC* pDC, double r1, double r2, double w);

	void DrawGround(CDC* pDC, float angle);
	void DrawCar(CDC* pDC, int x, int y, int w, int h, float anlge);
	void DrawWheel(CDC* pDC, int x, int y, int r, float angle);
	void DrawWP(CDC* pDC, double r1, float r2, float w, float L, float alpha, float d);
	void DrawFigureArc(CDC* pDC, int size, COLORREF colFill, COLORREF colLin);
	void DrawComplexFigureArc(CDC* pDC, int size, COLORREF colFill[], COLORREF colLine, double ratio, CString str);
	void DrawPictureArc(CDC* pDC, int size, COLORREF colFill[], COLORREF colLine, double ratio, int row, int col);
	void DrawLake(CDC* pDC, CString strParts, CPoint ptStart, int radius, COLORREF colFill, COLORREF colLine, CString strText, CPoint ptTextCenter, COLORREF colText, float angleRotation);
	void DrawTop(CDC* pDC, int size);
	void DrawTop2(CDC* pDC, int size);
	void DrawTail(CDC* pDC, int size, int count, double alpha);
	void DrawRosete(CDC* pDC, int x, int y, int size, int count);
	void DrawHouse(CDC* pDC, float dx);
	void DrawMenu(CDC* pDC, float dx);
	void DrawArrow(CDC* pDC, float dx);
	void DrawMagnifier(CDC* pDC, float dx);
	void DrawMultiCircle(CDC* pDC, int radius, int nConc, COLORREF colFill, CString strText);
	void Tint(COLORREF &color, COLORREF target, float fraction);
	COLORREF InverementColor(COLORREF souceColor, COLORREF targetColor, float fraction);
	void DrawFunnyCircle(CDC* pDC, int radius, int nCircle, double dAlpha, int nConc, COLORREF colFillBig, COLORREF colFillSmall, CString strText);
	void DrawBitmap(CDC* pDC, int x, int y, float scale, CString name);//
	void DrawPicture(CDC* pDC, CRect rect, CString picName);//
	CDC* CreateGraientBitmap(CDC* pDC, double w, double h, COLORREF col1, COLORREF col2);
	void DrawStar(CDC *pDC, CDC* bmpDC, int N, int R1, int R2, int Xc, int Yc);
	void DrawBackground(CDC *pDC, CPoint ptCenter, int radius, CString strPicture);
	void DrawCoin(CDC *pDC, CPoint ptCenter, int radius, CString strText, int fsizeText, CString strCoin, int fsizeCoin, COLORREF clrText);
	void SaveBMP(CString name, CDC* pDC, CPoint ptCenter, int radius);
	void RotateAndSaveImage(CDC* pDC, CString fileName, double alpha);
	void DrawPieChart(CDC* pDC, CPoint ptCenter, int radius, float arValues[], int nValues, COLORREF arColors[]);
	void DrawPie(CDC* pDC, CRect rect, float startAngle, float endAngle, COLORREF clrPie, bool showNumber);
	void NacrtajISnimi(CDC* pDC, CRect rect, CString str);


	//havana
	void DrawHavanaField(CDC* pDC, CPoint ptCenter, float sideLen, COLORREF tile);
	void DrawHavanaBoard(CDC* pDC, CPoint topLeft, int sideElements, float sideLen);
	void DrawHavanaBackground(CDC* pDC, CPoint topLeft, CPoint bottomRight, CString strPicture);
// Overrides
public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:

// Implementation
public:
	virtual ~CGDITrainingView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
};

#ifndef _DEBUG  // debug version in GDITrainingView.cpp
inline CGDITrainingDoc* CGDITrainingView::GetDocument() const
   { return reinterpret_cast<CGDITrainingDoc*>(m_pDocument); }
#endif

