#include "stdafx.h"

#ifndef SHARED_HANDLERS
#include "GDITraining.h"
#endif

#include "GDITrainingDoc.h"
#include "GDITrainingView.h"

#define toRad 0.01745

// CGDITrainingView construction/destruction

void CGDITrainingView::DrawBoard(CDC* pDC, float from_X, float from_Y, float to_X, float to_Y)
{
	XFORM oldXForm;

	CFont font;
	font.CreateFontW(15, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, CString("Arial"));
	CFont* oldFont = pDC->SelectObject(&font);

	CBrush* frameDarkBrown = new CBrush(RGB(107,34,3));
	CBrush* tablebackgroundWhite = new CBrush(RGB(255, 255, 255));
	CBrush* clockBrightYellow = new CBrush(RGB(255, 255, 0));
	CBrush* oldBrush = pDC->SelectObject(frameDarkBrown);

	CPen* letterYellow = new CPen(PS_SOLID, 1, RGB(204, 204, 0));
	CPen* oldPen;

	//okvir spolja
	pDC->RoundRect(from_X, from_Y, to_X, to_Y, 10, 10);

	//okvir table
	pDC->SelectObject(tablebackgroundWhite);
	pDC->Rectangle(from_X + 20, from_Y + 20, to_X - 20, to_Y - 20);

	float boardWidth = (to_X - from_X) - 40;
	float boardHeight = (to_Y - from_Y) - 40;

	float fieldWidht = boardWidth / 9;
	float fieldHeight = boardHeight / 9;

	PrimeniTransformaciju(pDC, from_X + 20, from_Y + 20, 0.0, 0.0, 0.0, &oldXForm);

	
	for (int i = 0; i < 9; i++)
	{
		for (int j = 0; j < 9; j++)
		{
			pDC->Rectangle(j*fieldWidht, i*fieldHeight, (j + 1)*fieldWidht, (i + 1)*fieldHeight);
		}
	}



	pDC->SetTextAlign(TA_CENTER | TA_BASELINE);
	for (int i = 0; i < 9; i++)
	{
		
		pDC->TextOut(i*fieldWidht + fieldWidht / 2, -7, CString(this->letters[i]));
	}


}