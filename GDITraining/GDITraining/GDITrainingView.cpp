
// GDITrainingView.cpp : implementation of the CGDITrainingView class
//

#include "stdafx.h"
// SHARED_HANDLERS can be defined in an ATL project implementing preview, thumbnail
// and search filter handlers and allows sharing of document code with that project.
#ifndef SHARED_HANDLERS
#include "GDITraining.h"
#endif

#include "GDITrainingDoc.h"
#include "GDITrainingView.h"
#include <vector>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define toRad 0.01745
#define M_PI 3.14159265359
// CGDITrainingView

IMPLEMENT_DYNCREATE(CGDITrainingView, CView)

BEGIN_MESSAGE_MAP(CGDITrainingView, CView)
	//Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, &CView::OnFilePrintPreview)
	ON_WM_LBUTTONDOWN()
	ON_WM_KEYDOWN()
	ON_WM_KEYDOWN()
	ON_WM_ERASEBKGND()
END_MESSAGE_MAP()

// CGDITrainingView construction/destruction

CGDITrainingView::CGDITrainingView() noexcept
{
	// TODO: add construction code here
	letters[0] = 'A';
	letters[1] = 'B';
	letters[2] = 'C';
	letters[3] = 'D';
	letters[4] = 'E';
	letters[5] = 'F';
	letters[6] = 'G';
	letters[7] = 'H';
	letters[8] = 'I';

	m_hPos = 0.0f;
	m_angle = 25.0f;

	m_pTocak = new DImage();
	m_pTocak->Load(CString("E:\\grafika\\GDI2014\\Wheel.png"));
	m_met = GetEnhMetaFile(CString("E:\\grafika\\GDI2014\\clio.emf"));

	wheelDistance = 10.0;
	d_alpha = 0.0;
}

CGDITrainingView::~CGDITrainingView()
{
	delete m_pTocak;
	DeleteEnhMetaFile(m_met);
}

BOOL CGDITrainingView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

// CGDITrainingView drawing

void CGDITrainingView::OnDraw(CDC* pDC)
{
	CGDITrainingDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	// TODO: add draw code for native data here
	/*
	CRect rect;
	GetClientRect(&rect);
	offsetX = 1000.00 / rect.right;
	offsetY = 800.00 / rect.bottom;

	int prevMode = SetGraphicsMode(pDC->m_hDC, GM_ADVANCED);

	pDC->SetMapMode(MM_ANISOTROPIC);
	pDC->SetWindowExt(1000, 800);
	pDC->SetViewportExt(rect.right, rect.bottom);
	this->DrawAxes(pDC);


	//Translate(pDC, 50, 100, false);

	//DrawWheel(pDC, 150, 120, 10);
	DrawWP(pDC, 60, 50, 10, 400, 30, wheelDistance);*/

	CRect rect;
	GetClientRect(&rect);

	CDC* pMemDC = new CDC();
	pMemDC->CreateCompatibleDC(pDC);
	CBitmap bmp;
	bmp.CreateCompatibleBitmap(pDC, rect.Width(), rect.Height());
	pMemDC->SelectObject(&bmp);
	CBrush brush(RGB(200, 220, 255));
	CBrush* pOldBrush = pMemDC->SelectObject(&brush);
	pMemDC->Rectangle(0, 0, rect.Width(), rect.Height());
	pMemDC->SelectObject(pOldBrush);

	XFORM transOld;
	int gm = pMemDC->SetGraphicsMode(GM_ADVANCED);
	pMemDC->GetWorldTransform(&transOld);
	this->DrawAxes(pMemDC);

#pragma region proba
	//DrawWP(pMemDC, 60, 50, 10, 400, 30, wheelDistance);
	//Translate(pMemDC, -60, -60, false);
	
	//radi
	/* Translate(pMemDC, 150, 150, false);
	Rotate(pMemDC, wheelDistance*toRad * 10, false);
	
	DrawWheel(pMemDC, 60, 50, 10);
	*/
#pragma endregion

	DrawWP(pMemDC, 60, 50, 10, 400, 30, wheelDistance);
	
	//Translate(pMemDC, 500, 500, false);
	//DrawField(pMemDC, CPoint(500,500), 50, RGB(255, 255, 255));
	DrawHavanaBoard(pMemDC, CPoint(500, 50), 10, 10);

 #pragma region proba222
	//Translate(pMemDC, 500, 125, false);
	//DrawFunnyCircle(pMemDC, 100, 6, d_alpha, 5, RGB(255, 0, 0), RGB(0, 255, 0), CString("Krug"));

	//DrawBitmap(pMemDC, 100, 100, 1, CString("univer.jpg"));
	//DrawPicture(pMemDC, CRect(0, 0, 256, 256), CString("pinguin.jpg"));

	//CDC* src = CreateGraientBitmap(pDC, 200, 200, RGB(255, 0, 0), RGB(0, 0, 255));
	//pMemDC->BitBlt(100, 100, 200, 200, src, 0, 0, SRCCOPY);

	//DrawStar(pMemDC, src, 8, 100, 40, 300, 100);

	//DrawBackground(pMemDC, CPoint(550, 120), 100, CString("grey.jpg"));
	
	//DrawCoin(pMemDC, CPoint(550, 120), 100, CString("Narodna banka Republike Srbije "), 25, CString("50"),100,RGB(0,0,0));
	
	/*
	CPoint ptCenter = CPoint(500, 300);
	float arValues[] = { 19,7,4,32,23,15 };
	COLORREF arColors[] = { RGB(255,255,0), RGB(0,255,255), RGB(255,0,255), RGB(0,255,0), RGB(255,0,0), RGB(0,0,255) };
	CString arPictures[] = { CString("tom.png") ,CString("silja.jpg"),CString("panter.jpg"),CString("dzeri.jpg"),CString("dusko.jpg"),CString("daca.png") };
	CString arNames[] = { CString("tom") ,CString("silja"),CString("panter"),CString("dzeri"),CString("dusko"),CString("daca") };
	*/

	//DrawPieChart(pMemDC, ptCenter, 150, arValues, 6, arColors);

	//SaveBMP(CString("coin_saved.bmp"), pMemDC, CPoint(550, 120), 100);
	//RotateAndSaveImage(pMemDC, CString("grey.jpg"), 77);

	//DrawPie(pMemDC, CRect(200, 100, 400, 300), 0.0, 45.0, RGB(255, 255, 0), false);

	//pMemDC->Pie(500, 500, 600, 600, 550, 600, 600, 550);
#pragma endregion

#pragma region  probaaa

	/*
	Translate(pMemDC, 600, 400, false);
	DrawHouse(pMemDC, 150);

	Translate(pMemDC, 200, 0, false);
	DrawMenu(pMemDC, 150);

	Translate(pMemDC, 100, 0, false);
	DrawArrow(pMemDC, 150);

	Translate(pMemDC, 300, 0, true);
	DrawMagnifier(pMemDC, 150);
	*/

	/*
	DrawRosete(pMemDC, 600, 400, 200, 8);
	*/


	/*
	Translate(pMemDC, 600, 500, false);
	DrawTail(pMemDC, 50, 4, -30);

	Translate(pMemDC, 250, 0, false);
	DrawTail(pMemDC, 50, 4, -45);

	Translate(pMemDC, 150, -25, false);
	DrawTail(pMemDC, 50, 4, 0);
	*/

	/*
	Translate(pMemDC, 600, 200, false);
	this->DrawTop(pMemDC, 150);

	
	Translate(pMemDC, 300, 0, false);
	DrawTop2(pMemDC, 50);
	*/

	//Translate(pMemDC, 600, 400, false);
	//DrawFigureArc(pMemDC, 200, RGB(255, 0, 0), RGB(0, 255, 0));

	/*
	COLORREF colFill[3];
	colFill[0] = RGB(255, 0, 0);
	colFill[1] = RGB(255, 255, 0);
	colFill[2] = RGB(255, 0, 255);
	
	DrawComplexFigureArc(pMemDC, 50, colFill, RGB(0, 255, 0), 1.25, CString("25"));
	*/

	//DrawPictureArc(pMemDC, 50, colFill, RGB(0, 255, 0), 1.25, 3, 3);

	//Translate(pMemDC, -600, -400, false);
#pragma endregion

	pMemDC->SetWorldTransform(&transOld);
	pMemDC->SetGraphicsMode(gm);

	pDC->BitBlt(0, 0, rect.Width(), rect.Height(), pMemDC, 0, 0, SRCCOPY);
	pMemDC->DeleteDC();
	delete pMemDC;

	/*
	CRect rect;
	GetClientRect(&rect);

	CDC* pMemDC = new CDC();
	pMemDC->CreateCompatibleDC(pDC);
	CBitmap bmp;
	bmp.CreateCompatibleBitmap(pDC, rect.Width(), rect.Height());
	pMemDC->SelectObject(&bmp);
	CBrush brush(RGB(200, 220, 255));
	CBrush* pOldBrush = pMemDC->SelectObject(&brush);
	pMemDC->Rectangle(0, 0, rect.Width(), rect.Height());
	pMemDC->SelectObject(pOldBrush);

	XFORM transOld;
	int gm = pMemDC->SetGraphicsMode(GM_ADVANCED);
	pMemDC->GetWorldTransform(&transOld);
	float angle = m_angle;
	

	this->DrawGround(pMemDC, angle);

	int carWidth = 450;
	int carHeight = carWidth / 2.5;
	int x = 0 + m_hPos;
	int y = 0; 
	int r = 38;
	float alpha = toRad * m_hPos / (2 * 3.14159265);

	pMemDC->GetWorldTransform(&transOld);

	Translate(pMemDC, carWidth / 2.0, -carHeight / 2.0 - r / 2.0, true);
	Rotate(pMemDC, -angle, true);
	Translate(pMemDC, 0.0, rect.Height(), true);

	DrawCar(pMemDC, x, y, carWidth, carHeight, 0.0f);

	DrawWheel(pMemDC, x - 155, y + 70, r, alpha);
	DrawWheel(pMemDC, x + 125, y + 70, r, alpha);

	pMemDC->SetWorldTransform(&transOld);
	pMemDC->SetGraphicsMode(gm);

	pDC->BitBlt(0, 0, rect.Width(), rect.Height(), pMemDC, 0, 0, SRCCOPY);
	pMemDC->DeleteDC();
	delete pMemDC;*/

	
}

void CGDITrainingView::DrawPie(CDC* pDC, CRect rect, float startAngle, float endAngle, COLORREF clrPie, bool showNumber)
{
	CBrush* brush = new CBrush(clrPie);
	CBrush* oldBrush = pDC->SelectObject(brush);

	float radius = (rect.BottomRight().x - rect.TopLeft().x) / 2;

	float xStart = 0.0;
	float yStart = 0.0;

	float xEnd = 0.0;
	float yEnd = 0.0;

	//start
	float tanStart = tan(startAngle);
	float ctanStart = 1 / tan(startAngle);

	if ((tanStart > -1.0) && (tanStart < 1.0))
	{
		if (ctanStart >= 0)
		{
			xStart = radius;
			yStart = tanStart * xStart;
		}
		else
		{
			xStart = -radius;
			yStart = tanStart * xStart;
		}
	}
	else
	{
		if (startAngle <= 180.0)
		{
			yStart = -radius;
			xStart = ctanStart * yStart;
		}
		else
		{
			yStart = radius;
			xStart = ctanStart * yStart;
		}
	}
	
	//end
	float tanEnd = tan(endAngle);
	float ctanEnd = 1 / tan(endAngle);

	if ((tanEnd > -1.0) && (tanEnd < 1.0))
	{
		if (ctanEnd >= 0)
		{
			xEnd = radius;
			yEnd = tanEnd * xEnd;
		}
		else
		{
			xEnd = -radius;
			yEnd = tanEnd * xEnd;
		}
	}
	else
	{
		if (endAngle <= 180.0)
		{
			yEnd = -radius;
			xEnd = ctanEnd * yEnd;
		}
		else
		{
			yEnd = radius;
			xEnd = ctanEnd * yEnd;
		}
	}

	pDC->Pie(rect, CPoint(xStart, yStart), CPoint(xEnd, yEnd));

	pDC->SelectObject(oldBrush);
	brush->DeleteObject();
}

void CGDITrainingView::DrawPieChart(CDC* pDC, CPoint ptCenter, int radius, float arValues[], int nValues, COLORREF arColors[])
{
	XFORM oldTrans;
	pDC->GetWorldTransform(&oldTrans);
	int prevBkMode = pDC->SetBkMode(TRANSPARENT);

	CPen* pen = new CPen(PS_SOLID, 3, RGB(0, 0, 10));
	CPen* oldPen = pDC->SelectObject(pen);

	CBrush* brush = new CBrush(arColors[0]);
	CBrush* oldBrush = pDC->SelectObject(brush);

	CFont font;
	font.CreateFontW(20, 0, 0, 0, FW_BOLD, 0, 0, 0, 0, 0, 0, 0, 0, CString("Arial"));
	CFont* oldFont = pDC->SelectObject(&font);

	float sum = 0.0;
	for (int i = 0; i < nValues; i++)
		sum += arValues[i];

	Translate(pDC, ptCenter.x, ptCenter.y, false);
	pDC->Ellipse(-radius, -radius, radius, radius);

	//sum : 360 = arVlues[i] : x
	for (int i = 0; i < nValues; i++)
	{
		float angle = (360.0 * arValues[i]) / sum;
		Rotate(pDC, angle, false);
		
		
		
		Rotate(pDC, -angle / 2, false);
		FloodFill(pDC->m_hDC, radius / 2, 0, RGB(0, 0, 10));
		
		CString str;
		str.AppendFormat(_T("%f"), arValues[i]);
		pDC->TextOutW(radius / 2, 0, str);

		Rotate(pDC, -angle / 2, false);

		if(i < nValues - 1)
			brush = new CBrush(arColors[i+1]);
	}

	pDC->SetWorldTransform(&oldTrans);
	pDC->SetBkMode(prevBkMode);
	pDC->SelectObject(oldPen);
	pDC->SelectObject(oldFont);
	pen->DeleteObject();
	delete pen;
}

///???
void CGDITrainingView::RotateAndSaveImage(CDC* pDC, CString fileName, double alpha)
{
	XFORM oldTrans;
	pDC->GetWorldTransform(&oldTrans);

	DImage* img = new DImage();
	img->Load(fileName);

	int oldWidth = img->Width();
	int oldHeight = img->Height();

	int newWidth = oldWidth * cos(alpha*toRad) + oldHeight * sin(alpha*toRad);
	int newHeight = oldWidth * sin(alpha*toRad) + oldHeight * cos(alpha*toRad);

	Translate(pDC, 550, 120, false);
	Rotate(pDC, alpha, false);
	img->Draw(pDC, CRect(0, 0, img->Width(), img->Height()), CRect(-oldWidth / 2, -oldHeight / 2, oldWidth / 2, oldHeight / 2));
	
	pDC->SetWorldTransform(&oldTrans);

	CDC* memDC = new CDC();
	memDC->CreateCompatibleDC(pDC);
	CBitmap bitmap;
	bitmap.CreateBitmap(newWidth, newHeight, 1, 32, NULL);
	memDC->SelectObject(&bitmap);
	memDC->BitBlt(0, 0, newWidth, newHeight, pDC, -newWidth / 2 + 300, -newHeight / 2 + 300, SRCCOPY);
	img = new DImage(bitmap);
	img->Save(CString("drugi.jpg"));


}

void CGDITrainingView::SaveBMP(CString name, CDC* pDC, CPoint ptCenter, int radius)
{
	CDC* memDC = new CDC();
	memDC->CreateCompatibleDC(pDC);
	CBitmap bmap;
	bmap.CreateBitmap(2 * radius, 2 * radius, 1, 32, NULL);
	memDC->SelectObject(&bmap);
	memDC->BitBlt(0, 0, 2*radius, 2 * radius, pDC, ptCenter.x - radius, ptCenter.y - radius, SRCCOPY);
	DImage* saveImage = new DImage(bmap);
	saveImage->Save(name);
}

//isctava novcic kruznog oblika poluprecnika radius bez ispune sa crnom linijom debljine 3.
//duz oboda unutar novcica je ispisan tekst strText fontom velicine fsizeText.
//u centru novcica strCoin podebljanjim fontom velicine fsizeCoin.
//teskt na novcicu treva da bude boje clrTet i ispisan fontom Arial. font je fiksne sirine
void CGDITrainingView::DrawCoin(CDC *pDC, CPoint ptCenter, int radius, CString strText, int fsizeText, CString strCoin, int fsizeCoin, COLORREF clrText)
{
	XFORM oldTrans;
	pDC->GetWorldTransform(&oldTrans);

	int length = strText.GetLength();

	CFont fontLetters;
	fontLetters.CreateFontW(fsizeText, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, CString("Arial"));
	CFont fontNumber;
	fontNumber.CreateFontW(fsizeCoin,  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, CString("Arial"));
	CFont* oldFont = pDC->SelectObject(&fontLetters);

	int prevBKMode = pDC->SetBkMode(TRANSPARENT);
	int prevTextAlign = SetTextAlign(pDC->m_hDC, TA_CENTER | TA_BASELINE);

	CPen* penCircle = new CPen(PS_SOLID, 3, RGB(0, 0, 0));
	CPen* oldPen = pDC->SelectObject(penCircle);

	pDC->Ellipse(ptCenter.x - radius, ptCenter.y - radius, ptCenter.x + radius, ptCenter.y + radius);
	DrawBackground(pDC, ptCenter, radius, CString("grey.jpg"));

	int prevTextColor = pDC->SetTextColor(clrText);

	Translate(pDC, ptCenter.x, ptCenter.y, false);

	pDC->SelectObject(&fontNumber);
	pDC->TextOutW(0, fsizeCoin / 3, strCoin);

	pDC->SelectObject(&fontLetters);

	Rotate(pDC, 200.0, false);
	float angleIncrement = 360.0 / length;
	for (int i = 0; i < length; i++)
	{
		Translate(pDC, radius - fsizeText, 0, false);
		Rotate(pDC, 90, false);
		pDC->TextOutW(0, 0, CString(strText[i]));
		Rotate(pDC, -90, false);
		Translate(pDC, -(radius - fsizeText), 0, false);
		Rotate(pDC, angleIncrement, false);
	}

	pDC->SetWorldTransform(&oldTrans);
	pDC->SelectObject(oldFont);
	pDC->SetBkMode(prevBKMode);
	pDC->SetTextAlign(prevTextAlign);
	pDC->SelectObject(oldPen);
	pDC->SetTextColor(prevTextColor);
	fontLetters.DeleteObject();
	fontNumber.DeleteObject();
	penCircle->DeleteObject();
	delete penCircle;

}

//ucitava sliku pod nazivom strPicture i iscrtava ja tako da bude 
//vidljiva samo unutar kruga poluprecnika radius sa centrom u tacki ptCenter
void CGDITrainingView::DrawBackground(CDC *pDC, CPoint ptCenter, int radius, CString strPicture)
{
	DImage* img = new DImage();
	img->Load(strPicture);

	CRgn rectRgn, ellipseRgn, combineRgn;
	
	rectRgn.CreateRectRgn(ptCenter.x - radius, ptCenter.y - radius, ptCenter.x + radius, ptCenter.y + radius);
	ellipseRgn.CreateEllipticRgn(ptCenter.x - radius, ptCenter.y - radius, ptCenter.x + radius, ptCenter.y + radius);
	combineRgn.CreateRectRgn(0, 0, 0, 0);
	
	combineRgn.CombineRgn(&rectRgn, &ellipseRgn, RGN_AND);
	pDC->SelectClipRgn(&combineRgn);

	img->Draw(pDC, CRect(0, 0, img->Width(), img->Height()), CRect(ptCenter.x - radius, ptCenter.y - radius, ptCenter.x + radius, ptCenter.y + radius));
	//pDC->SelectClipRgn(NULL, RGN_COPY);
}

void CGDITrainingView::DrawStar(CDC *pDC, CDC* bmpDC, int N, int R1, int R2, int Xc, int Yc)
{
	CPoint* points = new CPoint[2 * N];
	
	float angle = 0.0;// -M_PI / N;
	for (int i = 0; i < 2 * N; i++)
	{
		if (i % 2 == 0)//spolja
		{
			points[i].x = Xc + R1 * cos(angle + M_PI/2);
			points[i].y = Yc + R1 * sin(angle + M_PI/2);
		}
		else//unutra
		{
			points[i].x = Xc + R2 * cos(angle + M_PI/2);
			points[i].y = Yc + R2 * sin(angle + M_PI/2);
		}

		angle += M_PI / N;
	}

	//pDC->Polygon(points, 2 * N);
	CRgn star;
	star.CreatePolygonRgn(points, 2 * N, WINDING);

	CRgn bitmap;
	bitmap.CreateRectRgn(Xc - R1, Yc - R1, Xc + R1, Yc + R1);

	CRgn finalRgn;
	finalRgn.CreateRectRgn(0, 0, 0, 0);
	finalRgn.CombineRgn(&star, &bitmap, RGN_AND);
	pDC->SelectClipRgn(&finalRgn);
	pDC->BitBlt(Xc - R1, Yc - R1, Xc + R1, Yc + R1, bmpDC, 0, 0, SRCCOPY);

	delete points;
}


CDC* CGDITrainingView::CreateGraientBitmap(CDC* pDC, double w, double h, COLORREF col1, COLORREF col2)
{
	CDC* pDC2 = new CDC();
	pDC2->CreateCompatibleDC(pDC);
	CBitmap* bmp = new CBitmap();
	w -= ((int)w % 2);
	bmp->CreateCompatibleBitmap(pDC, w, h);

	BITMAP bm;
	bmp->GetBitmap(&bm);

	int size = bm.bmHeight*bm.bmWidthBytes;
	byte * bitBuffer = new byte[size];

	byte** bitMatrix = new byte*[bm.bmHeight];
	for (int i = 0; i < bm.bmHeight; i++)
		bitMatrix[i] = new byte[bm.bmWidthBytes];


	byte r1 = GetRValue(col1);
	byte g1 = GetGValue(col1);
	byte b1 = GetBValue(col1);

	byte r2 = GetRValue(col2);
	byte g2 = GetGValue(col2);
	byte b2 = GetBValue(col2);

	
	float fractionIncrement = 1.0 / bm.bmHeight*3;
	for (int i = 0; i < bm.bmHeight; i++)
	{
		col1 = InverementColor(col1, col2, fractionIncrement);

		for (int j = 0; j < bm.bmWidthBytes; j = j + 4)
		{
			bitMatrix[i][j] = GetRValue(col1);
			bitMatrix[i][j + 1] = GetGValue(col1);
			bitMatrix[i][j + 2] = GetBValue(col1);
			bitMatrix[i][j + 3] = 0;	
		}

		
	}

	int k = 0;
	for (int i = 0; i < bm.bmHeight; i++)
	{
		for (int j = 0; j < bm.bmWidthBytes; j++)
		{
			bitBuffer[k++] = bitMatrix[i][j];
		}
	}

	bmp->SetBitmapBits(size, bitBuffer);
	delete[] bitBuffer;

	pDC2->SelectObject(bmp);

	return pDC2;

}

void CGDITrainingView::DrawPicture(CDC* pDC, CRect rect, CString picName)
{
	DImage* picture = new DImage();
	picture->Load(picName);

	CRect pictureRect(0, 0, picture->Width(), picture->Height());

	picture->Draw(pDC, pictureRect, rect);

	delete picture;
}

void CGDITrainingView::DrawBitmap(CDC* pDC, int x, int y, float scale, CString name)
{
	XFORM oldTrans;
	GetWorldTransform(pDC->m_hDC, &oldTrans);

	DImage* k = new DImage();
	k->Load(name);

	int bw = k->Width();
	int bh = k->Height();

	Translate(pDC, -(bw / 2 + x), -(bh / 2 + y), false);
	Scale(pDC, scale, scale, false);
	Translate(pDC, (bw*scale / 2 + x), (bh*scale / 2 + y), false);

	k->Draw(pDC, CRect(0, 0, k->Width(), k->Height()), CRect(x, y, (x + k->Width()), (y + k->Height())));

	SetWorldTransform(pDC->m_hDC, &oldTrans);
}

void CGDITrainingView::NacrtajISnimi(CDC* pDC, CRect rect, CString str)
{
	CBitmap* bitmap = new CBitmap();
	bitmap->CreateBitmap(200, 200, 1, 32, NULL);
	
	
}

//dAlpha ugao rotacije manjih krugova
//nCircle broj manjih krugova
void CGDITrainingView::DrawFunnyCircle(CDC* pDC, int radius, int nCircle, double dAlpha, int nConc, COLORREF colFillBig, COLORREF colFillSmall, CString strText)
{
	float angle = 360 / nCircle;

	Rotate(pDC, dAlpha, false);
	DrawMultiCircle(pDC, radius, nConc, colFillBig, strText);
	
	//prvi
	Translate(pDC, radius, 0, false);
	Rotate(pDC, -dAlpha, false);

	DrawMultiCircle(pDC, 0.2*radius, 2, colFillSmall, CString("1"));

	Rotate(pDC, dAlpha, false);
	Translate(pDC, -radius, 0, false);

	//ostali
	for (int i = 1; i < nCircle; i++)
	{
		Rotate(pDC, -i*angle, false);
		Translate(pDC, radius, 0, false);
		Rotate(pDC, i*angle-dAlpha, false);

		CString str;
		str.AppendFormat(_T("%d"), i + 1);
		DrawMultiCircle(pDC, 0.2*radius, 2, colFillSmall, str);

		Rotate(pDC, -i*angle+dAlpha, false);
		Translate(pDC, -radius, 0, false);
		Rotate(pDC, i*angle, false);
	}
	
}

//iscrtava niz od nConc koncentricnih krugova sa tekstom strText u sredini
//najveci krug je poluprecnika radius dok je svaki sledeci za 10% manji
//boja najveceg kruga je colFill a svaki sledeci je za 5% svetliji
//text font Arial velicina 12 podebljano
void CGDITrainingView::DrawMultiCircle(CDC* pDC, int radius, int nConc, COLORREF colFill, CString strText)
{
	CBrush* brush = new CBrush(colFill);
	CBrush* oldBrush = pDC->SelectObject(brush);

	double tenPercent = radius / 10;


	pDC->Ellipse(-radius, -radius, radius, radius);
	for (int i = 0; i < nConc - 1; i++)
	{
		radius -= tenPercent;
		Tint(colFill, RGB(255, 255, 255), 0.10);
		brush = new CBrush(colFill);
		pDC->SelectObject(brush);

		pDC->Ellipse(-radius, -radius, radius, radius);
	}

	CFont font;
	CFont* oldFont;
	font.CreateFontW(12, 0, 0, 0, FW_BOLD, 0, 0, 0, 0, 0, 0, 0, 0, CString("Arial"));
	oldFont = pDC->SelectObject(&font);

	pDC->SetTextAlign(TA_CENTER | TA_BASELINE);
	pDC->TextOutW(0, 0, strText);
	

	pDC->SelectObject(oldFont);
	pDC->SelectObject(oldBrush);
	brush->DeleteObject();
	font.DeleteObject();
}

COLORREF CGDITrainingView::InverementColor(COLORREF souceColor, COLORREF targetColor, float fraction)
{
	if (fraction > 1.0)
		return targetColor;

	COLORREF retColor;// = COLORREF(souceColor);

	*((BYTE*)(&retColor) + 0) = GetRValue(souceColor) + (GetRValue(targetColor) - GetRValue(souceColor)) * fraction;
	*((BYTE*)(&retColor) + 1) = GetGValue(souceColor) + (GetGValue(targetColor) - GetGValue(souceColor)) * fraction;
	*((BYTE*)(&retColor) + 2) = GetBValue(souceColor) + (GetBValue(targetColor) - GetBValue(souceColor)) * fraction;

	// *((BYTE*)(&retColor) + 0) = *((BYTE*)(&souceColor) + 0) +  (*((BYTE*)(&targetColor) + 0) - *((BYTE*)(&souceColor) + 0)) * fraction;
	// *((BYTE*)(&retColor) + 1) = *((BYTE*)(&souceColor) + 1) +  (*((BYTE*)(&targetColor) + 1) - *((BYTE*)(&souceColor) + 1)) * fraction;
	// *((BYTE*)(&retColor) + 2) = *((BYTE*)(&souceColor) + 2) +  (*((BYTE*)(&targetColor) + 2) - *((BYTE*)(&souceColor) + 2)) * fraction;

	 return retColor;
}

void CGDITrainingView::Tint(COLORREF &color, COLORREF target, float fraction)
{
	ASSERT(fraction <= 1.0);

	*((BYTE*)(&color) + 0) += (*((BYTE*)(&target) + 0) - *((BYTE*)(&color) + 0)) * fraction;
	*((BYTE*)(&color) + 1) += (*((BYTE*)(&target) + 1) - *((BYTE*)(&color) + 1)) * fraction;
	*((BYTE*)(&color) + 2) += (*((BYTE*)(&target) + 2) - *((BYTE*)(&color) + 2)) * fraction;
}

void CGDITrainingView::DrawMagnifier(CDC* pDC, float dx)
{
	XFORM oldTrans;
	pDC->GetWorldTransform(&oldTrans);

	CPen* penWhite = new CPen(PS_SOLID, 2, RGB(255, 0, 0));
	CPen* oldPen = pDC->SelectObject(penWhite);

	Translate(pDC, -dx / 2, -dx / 2, false);

	pDC->MoveTo(0.51*dx, 0.51*dx);
	pDC->LineTo(dx, dx);
	pDC->MoveTo(0, 0);

	pDC->Ellipse(0, 0, 0.6*dx, 0.6*dx);

	pDC->SetWorldTransform(&oldTrans);
	pDC->SelectObject(oldPen);
	penWhite->DeleteObject();
}

//iscrtava strelicu, strelica predstavlja luk velicine cetvrtine kruga poluprecnika dx
//na cijem kraju je  strelica nacrtana kao dve linije cije kranje tacke su pomerene 
//za 0.2*dx po x osi i 0.1*dx po y osi
void CGDITrainingView::DrawArrow(CDC* pDC, float dx)
{
	XFORM oldTrans;
	pDC->GetWorldTransform(&oldTrans);

	CPen* penWhite = new CPen(PS_SOLID, 2, RGB(255, 0, 0));
	CPen* oldPen = pDC->SelectObject(penWhite);

	Translate(pDC, dx / 2, 0.55*dx, false);

	pDC->SetArcDirection(AD_CLOCKWISE);
	pDC->Arc(-dx, -dx, dx, dx, 0, -dx, dx, 0);

	POINT arrowTop[3];
	arrowTop[0] = CPoint(0.2*dx, -dx - 0.1*dx);
	arrowTop[1] = CPoint(0, -dx);
	arrowTop[2] = CPoint(0.2*dx, -dx + 0.1*dx);
	
	pDC->Polyline(arrowTop, 3);

	pDC->SetWorldTransform(&oldTrans);
	pDC->SelectObject(oldPen);
	penWhite->DeleteObject();
}

//iscrtava 4 pravougaonika jedan preko drugog visine 0.25*dx i sirine dx
void CGDITrainingView::DrawMenu(CDC* pDC, float dx)
{
	CPen* penWhite = new CPen(PS_SOLID, 2, RGB(255, 0, 0));
	CPen* oldPen = pDC->SelectObject(penWhite);

	pDC->Rectangle(-dx / 2, -0.25*dx, dx / 2, 0);
	pDC->Rectangle(-dx / 2, -0.5*dx, dx / 2, -0.25*dx);

	pDC->Rectangle(-dx / 2, 0, dx / 2, 0.25*dx);
	pDC->Rectangle(-dx / 2, 0.25*dx, dx / 2, 0.5*dx);

	pDC->SelectObject(oldPen);
	penWhite->DeleteObject();
}

//iscrtava kucicu visine dx, sirina krova je dx, a sirina osnove je 0.8*dx, visina krova je 0.2
void CGDITrainingView::DrawHouse(CDC* pDC, float dx)
{
	CPen* penWhite = new CPen(PS_SOLID, 2, RGB(255, 0, 0));
	CPen* oldPen = pDC->SelectObject(penWhite);

	POINT house[4];
	house[0] = CPoint(-0.4*dx, -0.4*dx);
	house[1] = CPoint(-0.4*dx, 0.4*dx);
	house[2] = CPoint(0.4*dx, 0.4*dx);
	house[3] = CPoint(0.4*dx, -0.4*dx);

	pDC->Polygon(house, 4);

	POINT roof[3];
	roof[0] = CPoint(-dx/2, -dx * 0.4);//dole levo
	roof[1] = CPoint(dx/2, -dx * 0.4);
	roof[2] = CPoint(0, -0.6*dx);

	pDC->Polygon(roof, 3);

	pDC->SelectObject(oldPen);
	penWhite->DeleteObject();

}

void CGDITrainingView::DrawRosete(CDC* pDC, int x, int y, int size, int count)
{
	XFORM oldTrans;
	pDC->GetWorldTransform(&oldTrans);

	double angle = -360.0 / count;
	CBrush* orangeBrush = new CBrush(RGB(255, 165, 0));
	CBrush* whiteBrush = new CBrush(RGB(255, 255, 255));
	CBrush* yellowBrush = new CBrush(RGB(0, 255, 255));
	
	CBrush* oldBrush = pDC->SelectObject(orangeBrush);

	Translate(pDC, x, y, false);
	Rotate(pDC, 90, false);

	pDC->Ellipse(-size, -size, size, size);

	pDC->SelectObject(whiteBrush);

	for (int i = 0; i < count; i++)
	{
		pDC->Ellipse(-size / 3, -size, size / 3, 0);
		Rotate(pDC, angle, false);
	}
	
	pDC->SelectObject(yellowBrush);
	pDC->Ellipse(-size / 3, -size / 3, size / 3, size / 3);

	pDC->SetWorldTransform(&oldTrans);
	pDC->SelectObject(oldBrush);
	orangeBrush->DeleteObject();
	whiteBrush->DeleteObject();
	yellowBrush->DeleteObject();
}

void CGDITrainingView::DrawTail(CDC* pDC, int size, int count, double alpha)
{
	XFORM oldTrans;
	pDC->GetWorldTransform(&oldTrans);

	CPen* pen = new CPen(PS_SOLID, 2, RGB(0, 0, 10));
	CPen* oldPen = pDC->SelectObject(pen);

	CBrush* brush = new CBrush(RGB(255, 0, 255));
	CBrush* oldBrush = pDC->SelectObject(brush);

	Translate(pDC, -size / 2, 0, false);
	Rotate(pDC, alpha, false);
	Translate(pDC, size, 0, false);

	for (int i = 0; i < count; i++)
	{
		pDC->Ellipse(-size / 2, -size / 4, size / 2, size / 4);
		Translate(pDC, size / 2, 0, false);
		Rotate(pDC, alpha, false);
		Translate(pDC, size / 2, 0, false);
	}

	Translate(pDC, -size / 4, 0, false);
	DrawTop2(pDC, size/4);

	pDC->SetWorldTransform(&oldTrans);
	pDC->SelectObject(oldBrush);
	pDC->SelectObject(oldPen);

	pen->DeleteObject();
	brush->DeleteObject();
}

void CGDITrainingView::DrawTop2(CDC* pDC, int size)
{
	XFORM oldTrans;
	pDC->GetWorldTransform(&oldTrans);

	Rotate(pDC, 90, false);
	Mirror(pDC, false, false);
	this->DrawTop(pDC, size);

	pDC->SetWorldTransform(&oldTrans);
}

//naradzasta boja okvir 2p crn
void CGDITrainingView::DrawTop(CDC* pDC, int size)
{
	XFORM oldTrans;
	pDC->GetWorldTransform(&oldTrans);

	CPen* pen = new CPen(PS_SOLID, 2, RGB(0, 0,10));
	CPen* oldPen = pDC->SelectObject(pen);

	CBrush* brush = new CBrush(RGB(255, 165, 0));
	CBrush* oldBrush = pDC->SelectObject(brush);

	pDC->SetArcDirection(AD_CLOCKWISE);

	pDC->Arc(-size, -size, size, size, 0, size, size, 0); //veci luk 3/4
	pDC->Arc(-size / 2, size, size / 2, 2 * size, 0, size, 0, 2 * size); //manji luk 1/2
	pDC->Arc(-size, -2 * size, size, 2 * size, size, 0, 0, 2 * size);

	pDC->FloodFill(0, 0, RGB(0, 0, 10));


	pDC->SetWorldTransform(&oldTrans);
	pDC->SelectObject(oldBrush);
	pDC->SelectObject(oldPen);
	pen->DeleteObject();
	brush->DeleteObject();
}

void CGDITrainingView::DrawLake(CDC* pDC, CString strParts, CPoint ptStart, int radius, COLORREF colFill, COLORREF colLine, 
								CString strText, CPoint ptTextCenter, COLORREF colText, float angleRotation)
{
	XFORM oldTrans;
	pDC->GetWorldTransform(&oldTrans);

	CPen* pen = new CPen(PS_SOLID, 2, colLine);
	CPen* oldPen = pDC->SelectObject(pen);

	CBrush* brush = new CBrush(colFill);
	CBrush* oldBrush = pDC->SelectObject(brush);

	std::vector<byte> direction = { 0,1,1,0   ,1,1 };
	

	Translate(pDC, ptStart.x, ptStart.y, false);
	pDC->MoveTo(0, 0);
	pDC->SetArcDirection(AD_CLOCKWISE);
	
	for (int i = 0; i < strParts.GetLength(); i++)
	{
		char c = strParts.GetAt(i);
		int cInt = c - '0';
		if (c == '1')
		{
			if (direction.at(cInt - 1) == 0)//- ok
			{
				pDC->Arc(-radius, 0, radius, 2 * radius, 0, 0, radius, radius);
				Translate(pDC, radius, radius, false);
				
				direction = { 0,0,0,0  ,1,1 };
			}
			else//+  ok
			{
				pDC->Arc(-2 * radius, -radius, 0, radius, -radius, -radius, 0, 0);
				Translate(pDC, -radius, -radius, false);
				
				direction = { 1,0,1,1   ,0,0 };
			}
			
			pDC->MoveTo(0, 0);
		}
		else if (c == '2')
		{
			if (direction.at(cInt - 1) == 0)//- ok
			{
				pDC->Arc(-radius, 0, radius, 2 * radius, -radius, radius, 0, 0);
				Translate(pDC, -radius, radius, false);
				
				direction = { 0,0,0,0  ,1,0};
			}
			else//+ ok
			{
				pDC->Arc(0, -radius, 2 * radius, radius, 0, 0, radius, -radius);
				Translate(pDC, radius, -radius, false);
				
				direction = { 0,1,1,0 ,1,1 };
			}

			pDC->MoveTo(0, 0);
		}
		else if (c == '3')
		{
			if (direction.at(cInt - 1) == 0)//- ok 
			{
				pDC->Arc(0,-radius,2*radius,radius,radius,radius,0,0);
				Translate(pDC, radius, radius, false);

				direction = { 1,0,0,1 , 0,0};
			}
			else//+ ok
			{
				pDC->Arc(-radius, -2 * radius, radius, 0, 0, 0, -radius, -radius);
				Translate(pDC, -radius, -radius, false);
				
				direction = { 1,1,1,1   ,0,0};
			}
			
			pDC->MoveTo(0, 0);
		}
		else if (c == '4')
		{
			if (direction.at(cInt - 1) == 0)//- ok
			{
				pDC->Arc(-2 * radius, -radius, 0, radius, 0, 0, -radius, radius);
				Translate(pDC, -radius, radius, false);
				
				direction = { 0,1,1,0 ,0,0};
			}
			else//+
			{
				pDC->Arc(-radius, 0, radius, -2*radius, radius, -radius, 0, 0);
				Translate(pDC, radius, -radius, false);

				direction = { 1,1,1,1 ,0,0};
			}
			
			
			pDC->MoveTo(0, 0);
		}
		else if (c == '5')
		{
			if (direction.at(cInt - 1) == 0)
			{
				pDC->LineTo(-radius, 0);
				Translate(pDC, -radius, 0, false);

				
				direction[1] = 0;
				direction[2] = 1;
			}
			else
			{
				pDC->LineTo(radius, 0);
				Translate(pDC, radius, 0, false);

				direction[0] = 0;
			}
			
			pDC->MoveTo(0, 0);
		}
		else if (c == '6')
		{
			if (direction.at(cInt - 1) == 0)
			{
				pDC->LineTo(0, -radius);
				Translate(pDC, 0, -radius, false);

				direction[3] = 0;
			}
			else
			{
				pDC->LineTo(0, radius);
				Translate(pDC, 0, radius, false);
			}

			pDC->MoveTo(0, 0);
		}
	}
	
	pDC->FloodFill(5,5, colLine);

	pDC->SelectObject(oldPen);
	pDC->SelectObject(oldBrush);
	pDC->SetWorldTransform(&oldTrans);

	CFont font;
	font.CreateFontW(30, 0, 0, 0, FW_BOLD, 0, 0, 0, 0, 0, 0, 0, 0, CString("Arial"));
	CFont* oldFont = pDC->SelectObject(&font);
	pDC->TextOutW(ptTextCenter.x, ptTextCenter.y, strText);

	pDC->SelectObject(oldFont);

	font.DeleteObject();
	pen->DeleteObject();
	brush->DeleteObject();

}

void CGDITrainingView::DrawPictureArc(CDC* pDC, int size, COLORREF colFill[], COLORREF colLine, double ratio, int row, int col)
{
	XFORM oldTrans;
	pDC->GetWorldTransform(&oldTrans);

	for (int i = 0; i < row; i++)
	{
		int numInRow;
		if (i % 2 == 0)
			numInRow = col;
		else
			numInRow = col - 1;

		for (int j = 0; j < numInRow; j++)
		{
			CString str;
			str.AppendFormat(_T("%d"), i+1);
			str.AppendFormat(_T("%d"), j+1);
			this->DrawComplexFigureArc(pDC, size, colFill, colLine, ratio, str);

			Translate(pDC, 4 * size / 2 * ratio * ratio, 0, false);
		}

		Translate(pDC, -5*(col - 1) * (size / 2 * ratio * ratio), 2 * (size / 2 * ratio * ratio), false);
	}

	pDC->SetWorldTransform(&oldTrans);
}

//size - velicina figure u sredini
//ratio - odnos velicina najvece i srednje tj srednje i najmanje 
void CGDITrainingView::DrawComplexFigureArc(CDC* pDC, int size, COLORREF colFill[], COLORREF colLine, double ratio, CString str)
{
	//veliki
	int sizeLarge = size * ratio * ratio;
	this->DrawFigureArc(pDC, sizeLarge, colFill[0], colLine);

	//srednji
	int sizeMedium = size * ratio;
	this->DrawFigureArc(pDC, sizeMedium, colFill[1], colLine);

	//mali
	this->DrawFigureArc(pDC, size, colFill[2], colLine);

	CFont font;
	font.CreateFontW(12, 0, 0, 0, FW_BOLD, 0, 0, 0, 0, 0, 0, 0, 0, CString("Arial"));
	CFont* oldFont = pDC->SelectObject(&font);
	pDC->TextOutW(-6, -6, str);

	pDC->SelectObject(oldFont);
	font.DeleteObject();
}

//size - velicina pomocnog kvadrata
//colFill - ispuna figure
//colLine - boja linije
void CGDITrainingView::DrawFigureArc(CDC* pDC, int size, COLORREF colFill, COLORREF colLin)
{
	CPen* linePen = new CPen(PS_SOLID, 2, colLin);
	CPen* oldPen = pDC->SelectObject(linePen);

	CBrush* brush = new CBrush(colFill);
	CBrush* oldBrush = pDC->SelectObject(brush);

	POINT topLeft, bottomRight, topRight, bottomLeft;
	topLeft.x = -size/2;
	topLeft.y = -size/2;
	bottomRight.x = size/2;
	bottomRight.y = size/2;
	topRight.x = size/2;
	topRight.y = -size / 2;
	bottomLeft.x = -size / 2;
	bottomLeft.y = size / 2;
	
	int sq = size / 4;

	CRect* rectTmp = new CRect(topLeft, bottomRight);

	pDC->SetArcDirection(AD_CLOCKWISE);
	pDC->Arc(topRight.x - sq, topRight.y - sq, topRight.x + sq, topRight.y + sq, topRight.x, topRight.y + sq, topRight.x - sq, topRight.y);
	pDC->Arc(topRight.x - sq, topRight.y + sq, bottomRight.x + sq, bottomRight.y - sq, topRight.x, topRight.y + sq, bottomRight.x, bottomRight.y - sq);
	pDC->Arc(bottomRight.x - sq, bottomRight.y - sq, bottomRight.x + sq, bottomRight.y + sq, bottomRight.x - sq, bottomRight.y, bottomRight.x, bottomRight.y - sq);
	pDC->Arc(bottomLeft.x + sq, bottomLeft.y - sq, bottomRight.x - sq, bottomRight.y + sq, bottomRight.x - sq, bottomRight.y, bottomLeft.x + sq, bottomLeft.y);
	pDC->Arc(bottomLeft.x - sq, bottomLeft.y - sq, bottomLeft.x + sq, bottomLeft.y + sq, bottomLeft.x, bottomLeft.y - sq, bottomLeft.x + sq, bottomLeft.y);
	pDC->Arc(topLeft.x - sq, topLeft.y + sq, bottomLeft.x + sq, bottomLeft.y - sq, bottomLeft.x, bottomLeft.y - sq, topLeft.x, topLeft.y - sq);
	pDC->Arc(topLeft.x - sq, topLeft.y - sq, topLeft.x + sq, topLeft.y + sq, topLeft.x + sq, topLeft.y, topLeft.x, topLeft.y + sq);
	pDC->Arc(topLeft.x + sq, topLeft.y - sq, topRight.x - sq, topRight.y + sq, topLeft.x + sq, topLeft.y, topRight.x - sq, topRight.y);

	pDC->FloodFill(0, 0, colLin);

	pDC->SelectObject(oldPen);
	pDC->SelectObject(oldBrush);
	linePen->DeleteObject();
	brush->DeleteObject();
}

//iscrtava kosu ravan sa nagibom alpha
//horizontalne duzine L i tockom na rastojanju d od vrha ravni
void CGDITrainingView::DrawWP(CDC* pDC, double r1, float r2, float w, float L, float alpha, float d)
{
	XFORM oldTrans;
	pDC->GetWorldTransform(&oldTrans);
	CBrush* brushBlue = new CBrush(RGB(0, 0, 255));
	CBrush* oldBrush = pDC->SelectObject(brushBlue);

	POINT pts[3];

	pts[0].x = 0;
	pts[0].y = r1;
	pts[1].x = 0;
	pts[1].y = r1 + L * tan(alpha * toRad);
	pts[2].x = L;
	pts[2].y = r1 + L * tan(alpha * toRad);

	pDC->Polygon(pts, 3);

	float rotateAngle = d / (2 * r1 * M_PI / 360.0);

	//wheel	
	Translate(pDC, d * cos(alpha * toRad), d * sin(alpha * toRad), false);
	Rotate(pDC, rotateAngle, false);

	DrawWheel(pDC, r1, r2, w);

	pDC->SetWorldTransform(&oldTrans);
	pDC->SelectObject(oldBrush);
	brushBlue->DeleteObject();
}

void CGDITrainingView::DrawWheel(CDC* pDC, double r1, double r2, double w)
{
	XFORM oldTrans;
	pDC->GetWorldTransform(&oldTrans);

	CBrush* brushRed = new CBrush(RGB(255, 0, 0));
	CBrush* brushWhite = new CBrush(RGB(255, 255, 255));
	CBrush* oldBrush = pDC->SelectObject(brushRed);
	
	//tocak spolja
	pDC->RoundRect(-r1, -r1, r1, r1, 2*r1, 2*r1);
	
	pDC->SelectObject(brushWhite);
	//tocak unutra
	pDC->RoundRect(-r2, -r2, r2, r2, 2*r2, 2*r2);
	
	pDC->SelectObject(brushRed);
	//precke
	Rotate(pDC, 45, false);
	
	pDC->Rectangle(-w / 2, -r2, w / 2, r2);

	Rotate(pDC, 45, false);

	pDC->Rectangle(-w / 2, -r2, w / 2, r2);

	Rotate(pDC, 45, false);

	pDC->Rectangle(-w / 2, -r2, w / 2, r2);

	pDC->SelectObject(oldBrush);
	brushRed->DeleteObject();
	brushWhite->DeleteObject();

	pDC->SetWorldTransform(&oldTrans);
}

void CGDITrainingView::DrawAxes(CDC* pDC)
{
	CPen* blackPen = new CPen(PS_SOLID, 1, RGB(0, 0, 0));
	CPen* redPen = new CPen(PS_SOLID, 1, RGB(255, 0, 0));
	CPen* oldPen = pDC->SelectObject(blackPen);

	//horizontalno
	for (int i = 0; i < 800; i = i + 50)
	{
		pDC->MoveTo(0, i);
		pDC->LineTo(1500, i);
	}

	//vertikalno
	for (int i = 0; i < 1500; i = i + 50)
	{
		pDC->MoveTo(i, 0);
		pDC->LineTo(i, 800);
	}

	pDC->SelectObject(oldPen);
	redPen->DeleteObject();
	blackPen->DeleteObject();
}

void CGDITrainingView::DrawRoom(CDC* pDC)
{
	CBrush* wallOrange = new CBrush(RGB(155,192,128));
	CBrush* oldBrush = pDC->SelectObject(wallOrange);

	//zadnji zid
	pDC->Rectangle(100, 100, 900, 700);

	CBrush* wallBrown = new CBrush(RGB(185, 140, 85));
	pDC->SelectObject(wallBrown);
	CPoint leftWall[4] = { CPoint(0,0), CPoint(0,800), CPoint(100,700), CPoint(100,100) };

	//levi zid
	pDC->Polygon(leftWall, 4);

	CPoint rightWall[4] = { CPoint(1000, 0), CPoint(900, 100), CPoint(900, 700), CPoint(1000, 800) };

	//desni zid
	pDC->Polygon(rightWall, 4);

	CBrush* floorGrey = new CBrush(RGB(200, 200, 200));
	pDC->SelectObject(floorGrey);

	CPoint floor[4] = { CPoint(100,700), CPoint(0,800), CPoint(1000,800), CPoint(900,700) };

	//pod
	pDC->Polygon(floor, 4);

	//inventar
	pDC->RoundRect(10, 10, 90, 90, 10, 10);

	pDC->SelectObject(oldBrush);
	wallBrown->DeleteObject();
	wallOrange->DeleteObject();
	floorGrey->DeleteObject();
}

void CGDITrainingView::LoadIdentity(CDC* pDC)
{
	XFORM tmpForm;

	pDC->ModifyWorldTransform(&tmpForm, MWT_IDENTITY);
}

void CGDITrainingView::Translate(CDC* pDC, float dX, float dY, bool rightMul)
{
	XFORM XForm;
	XForm.eM11 = 1.0;
	XForm.eM12 = 0.0;
	XForm.eM21 = 0.0;
	XForm.eM22 = 1.0;
	XForm.eDx = dX;
	XForm.eDy = dY;
	
	if (rightMul)
		pDC->ModifyWorldTransform(&XForm, MWT_RIGHTMULTIPLY);
	else
		pDC->ModifyWorldTransform(&XForm, MWT_LEFTMULTIPLY);
}

void CGDITrainingView::Rotate(CDC* pDC, float angle, bool rightMul)
{
	XFORM XForm;
	XForm.eM11 = cos(angle*toRad);
	XForm.eM12 = sin(angle*toRad);
	XForm.eM21 = -sin(angle*toRad);
	XForm.eM22 = cos(angle*toRad);
	XForm.eDx = 0.0;
	XForm.eDy = 0.0;

	if (rightMul)
		pDC->ModifyWorldTransform(&XForm, MWT_RIGHTMULTIPLY);
	else
		pDC->ModifyWorldTransform(&XForm, MWT_LEFTMULTIPLY);
}

void CGDITrainingView::Scale(CDC* pDC, float sX, float sY, bool rightMul)
{
	XFORM XForm;
	XForm.eM11 = sX;
	XForm.eM12 = 0.0;
	XForm.eM21 = 0.0;
	XForm.eM22 = sY;
	XForm.eDx = 0.0;
	XForm.eDy = 0.0;

	if (rightMul)
		pDC->ModifyWorldTransform(&XForm, MWT_RIGHTMULTIPLY);
	else
		pDC->ModifyWorldTransform(&XForm, MWT_LEFTMULTIPLY);
}

//vrsi refleksiju u odnosu na x osu ako je parametar x true u supronom u odnosu na y osu
//parametar right odredje da li se mnozenje matrica obavlja sa desne (true) ili sa leve strane (false)
void CGDITrainingView::Mirror(CDC* pDC, bool x, bool right)
{
	XFORM XForm;
	if (x)
	{
		XForm.eM11 = -1.0;
		XForm.eM12 = 0.0;
		XForm.eM21 = 0.0;
		XForm.eM22 = 1.0;
		XForm.eDx = 0.0;
		XForm.eDy = 0.0;
	}
	else
	{
		XForm.eM11 = 1.0;
		XForm.eM12 = 0.0;
		XForm.eM21 = 0.0;
		XForm.eM22 = -1.0;
		XForm.eDx = 0.0;
		XForm.eDy = 0.0;
	}

	if (right)
		pDC->ModifyWorldTransform(&XForm, MWT_RIGHTMULTIPLY);
	else
		pDC->ModifyWorldTransform(&XForm, MWT_LEFTMULTIPLY);
}

void CGDITrainingView::PrimeniTransformaciju(CDC* pDC, float cx, float cy, float rx, float ry, float alfa, XFORM* oldXForm)
{
	XFORM XForm;

	int prevMode = SetGraphicsMode(pDC->m_hDC, GM_ADVANCED);
	bool b = GetWorldTransform(pDC->m_hDC, oldXForm);

	XForm.eM11 = 1.0;
	XForm.eM12 = 0.0;
	XForm.eM21 = 0.0;
	XForm.eM22 = 1.0;
	XForm.eDx = rx;
	XForm.eDy = ry;

	b = SetWorldTransform(pDC->m_hDC, &XForm);

	XForm.eM11 = cos(alfa*toRad);
	XForm.eM12 = sin(alfa*toRad);
	XForm.eM21 = -sin(alfa*toRad);
	XForm.eM22 = cos(alfa*toRad);
	XForm.eDx = 0.0;
	XForm.eDy = 0.0;

	b = ModifyWorldTransform(pDC->m_hDC, &XForm, MWT_RIGHTMULTIPLY);

	XForm.eM11 = 1.0;
	XForm.eM12 = 0.0;
	XForm.eM21 = 0.0;
	XForm.eM22 = 1.0;
	XForm.eDx = cx;
	XForm.eDy = cy;

	b = ModifyWorldTransform(pDC->m_hDC, &XForm, MWT_RIGHTMULTIPLY);

	SetGraphicsMode(pDC->m_hDC, prevMode);
}

// CGDITrainingView diagnostics

#ifdef _DEBUG
void CGDITrainingView::AssertValid() const
{
	CView::AssertValid();
}

void CGDITrainingView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CGDITrainingDoc* CGDITrainingView::GetDocument() const // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CGDITrainingDoc)));
	return (CGDITrainingDoc*)m_pDocument;
}
#endif //_DEBUG


// CGDITrainingView message handlers


void CGDITrainingView::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	// TODO: Add your message handler code here and/or call default

	if (nChar == VK_RIGHT)
	{
		d_alpha += 10.0;
		m_hPos += 10.0;
		wheelDistance += 20.0;
		Invalidate();
	}

	if (nChar == VK_LEFT)
	{
		d_alpha -= 10.0;
		m_hPos -= 10.0;
		wheelDistance -= 20.0;
		if (m_hPos < 0.0)
			m_hPos = 0.0;
		Invalidate();
	}

	if (nChar == VK_UP)
	{
		m_hPos = 0.0;
		m_angle += 10.0;
		if (m_angle > 80.0)
			m_angle = 80.0;
		Invalidate();
	}

	if (nChar == VK_DOWN)
	{
		m_hPos = 0.0;
		m_angle -= 10.0;
		if (m_angle < -10.0)
			m_angle = -10.0;
		Invalidate();
	}

	CView::OnKeyDown(nChar, nRepCnt, nFlags);
}


BOOL CGDITrainingView::OnEraseBkgnd(CDC* pDC)
{
	// TODO: Add your message handler code here and/or call default
	
	return TRUE;
}
