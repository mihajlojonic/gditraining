
// GDITraining.h : main header file for the GDITraining application
//
#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"       // main symbols


// CGDITrainingApp:
// See GDITraining.cpp for the implementation of this class
//

class CGDITrainingApp : public CWinApp
{
public:
	CGDITrainingApp() noexcept;


// Overrides
public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();

// Implementation
	UINT  m_nAppLook;
	afx_msg void OnAppAbout();
	DECLARE_MESSAGE_MAP()
};

extern CGDITrainingApp theApp;
