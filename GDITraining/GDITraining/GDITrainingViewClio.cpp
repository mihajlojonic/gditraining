#include "stdafx.h"

#ifndef SHARED_HANDLERS
#include "GDITraining.h"
#endif

#include "GDITrainingDoc.h"
#include "GDITrainingView.h"

#define toRad 0.01745

void CGDITrainingView::DrawGround(CDC* pDC, float angle)
{
	CRect rect;
	GetClientRect(&rect);

	CPen pen(PS_SOLID, 3, RGB(128, 32, 32));
	CPen *oldPen = pDC->SelectObject(&pen);
	
	CBrush brush(HS_FDIAGONAL, RGB(128, 32, 32));
	CBrush* oldBrush = pDC->SelectObject(&brush);

	POINT pts[3];
	pts[0].x = rect.left; 
	pts[0].y = rect.bottom;
	pts[1].x = rect.right;
	pts[1].y = rect.bottom;
	pts[2].x = rect.right;
	pts[2].y = rect.bottom - rect.Width()*tan(angle*toRad);

	COLORREF oldBkColor = pDC->SetBkColor(RGB(220, 155, 192));

	pDC->Polygon(pts, 3);

	pDC->SetBkColor(oldBkColor);
	pDC->SelectObject(oldPen);
	pDC->SelectObject(oldBrush);
}

//prikazije metafajl clio.emf sa centrom u tacki (x,y) sirine w i visine h
//i automobil je okrenut u desno
void CGDITrainingView::DrawCar(CDC* pDC, int x, int y, int w, int h, float anlge)
{
	pDC->PlayMetaFile(m_met, CRect(x + w / 2, y - h / 2, x - w / 2, y + h / 2));
}

//iscrtava Wheel.png sa centrom u tacki (x,y), poluprecnika r i zarotiran
//za ugao angle u stepenima
//tocak se na slici wheel.png nalazi na poziciji definisamo 
//okvirnim pravougaonikom sa gornjim levim uglom na poziciji (52,15) 
//i sirine/visine jednake 376 piksela. iscrtavanje ograniciti samo 
//na dati okvirni pravougaonik
//za iscrtavanje koristiti funkciju DrawTransparent klase DImage, 
//koja isctava datu bitmapu uz transparenciju zadate boje
void CGDITrainingView::DrawWheel(CDC* pDC, int x, int y, int r, float angle)
{
	XFORM transOld;
	pDC->GetWorldTransform(&transOld);

	Translate(pDC, x, y, false);//left mul
	Rotate(pDC, angle, false);//left mul

	int dx = 52;
	int dy = 15; 
	int w = 376;

	CRect srcRc(dx, dy, dx + w, dy + w);
	CRect dstRc(-r, -r, r, r);
	m_pTocak->Draw(pDC, srcRc, dstRc);
	pDC->SetWorldTransform(&transOld);
}
